package com.fashionTrade.models.dresses.document;

import com.fashionTrade.models.dresses.payload.ImagePayload;
import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

public class ImageTest {

    private static final String LARGE_URL = "www.large-image-url.com";
    private static final String THUMB_URL = "www.thumb-image-url.com";

    @Test
    public void shouldBeBuildFromPayloadObject() {
        ImagePayload payload = new ImagePayload(LARGE_URL, THUMB_URL);

        Image image = new Image(payload);

        assertThat(image.getLargeUrl(), is(LARGE_URL));
        assertThat(image.getThumbUrl(), is(THUMB_URL));
    }
}