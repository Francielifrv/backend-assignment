package com.fashionTrade.models.dresses.document;

import com.fashionTrade.models.dresses.payload.BrandPayload;
import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

public class BrandTest {

    private static final String BRAND_NAME = "brand_name";
    private static final String LOGO_URL = "logo_url";

    @Test
    public void shouldBeBuiltFromPayloadObject() {
        BrandPayload payload = new BrandPayload(BRAND_NAME, LOGO_URL);

        Brand brand = new Brand(payload);

        assertThat(brand.getName(), is(BRAND_NAME));
        assertThat(brand.getLogoUrl(), is(LOGO_URL));
    }
}