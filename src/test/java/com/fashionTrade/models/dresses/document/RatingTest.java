package com.fashionTrade.models.dresses.document;

import com.fashionTrade.models.ratings.Rating;
import com.fashionTrade.models.ratings.RatingPayload;
import org.junit.Test;

import java.util.Date;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class RatingTest {

    private static final String RATING_ID = "rating_id";
    private static final String DRESS_ID = "dress_id";
    private static final int STARS = 3;
    private static final Long TIMESTAMP = 1487596717302L;

    @Test
    public void shouldBeBuiltFromPayloadObject() {
        RatingPayload ratingPayload = new RatingPayload(RATING_ID, DRESS_ID, STARS);

        Rating rating = new Rating(ratingPayload, TIMESTAMP);

        assertThat(rating.getRatingId(), is(RATING_ID));
        assertThat(rating.getStars(), is(STARS));
        assertThat(rating.getRatedAt(), is(new Date(TIMESTAMP)));
    }
}