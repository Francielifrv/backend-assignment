package com.fashionTrade.models.dresses.document;

import com.fashionTrade.models.dresses.Season;
import com.fashionTrade.models.dresses.payload.BrandPayload;
import com.fashionTrade.models.dresses.payload.DressPayload;
import com.fashionTrade.models.dresses.payload.ImagePayload;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class DressTest {
    private static final String DRESS_ID = "9fzZBWUBaM7qrfQDzrmU";

    private static final Date ACTIVATION_DATE = new Date();

    private static final BigDecimal PRICE = new BigDecimal("99.90");
    private static final String DRESS_NAME = "dress name";
    private static final String DRESS_COLOR = "dress color";
    private static final String BRAND_NAME = "Brand name";
    private static final String BRAND_LOGO = "www.brand-logo.com";

    private static final DressPayload DRESS_PAYLOAD = createDressPayload();

    @Test
    public void shouldBeBuiltFromPayloadObject() {
        Dress dress = new Dress(DRESS_PAYLOAD);

        assertThat(dress.getName(), is(DRESS_NAME));
        assertThat(dress.getSeason(), is(Season.AUTUMN));
        assertThat(dress.getColor(), is(DRESS_COLOR));
        assertThat(dress.getId(), is(DRESS_ID));
        assertThat(dress.getActivationDate(), is(ACTIVATION_DATE));
        assertThat(dress.getPrice(), is(PRICE));
        assertThat(dress.getImages(), hasSize(2));
        assertThat(dress.getBrand(), is(not(nullValue())));
    }

    private static DressPayload createDressPayload() {
        ImagePayload image = new ImagePayload("large_url", "thumb_url");
        ImagePayload anotherImage = new ImagePayload("some_ulr", "some_thumb_url");

        List<ImagePayload> images = Arrays.asList(image, anotherImage);
        BrandPayload brand = new BrandPayload(BRAND_NAME, BRAND_LOGO);

        return new DressPayload(images, ACTIVATION_DATE, DRESS_NAME, DRESS_COLOR, Season.AUTUMN, PRICE, brand, DRESS_ID);
    }
}