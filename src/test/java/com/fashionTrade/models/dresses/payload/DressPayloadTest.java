package com.fashionTrade.models.dresses.payload;

import com.fashionTrade.models.dresses.Season;
import com.fashionTrade.models.dresses.document.Brand;
import com.fashionTrade.models.dresses.document.Dress;
import com.fashionTrade.models.dresses.document.Image;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Date;

import static java.util.Collections.singletonList;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class DressPayloadTest {
    @Test
    public void shouldConvertDomainObjectToPayload() {
        Dress dress = buildDress();

        DressPayload payload = new DressPayload(dress);

        assertThat(payload.getId(), is(dress.getId()));
        assertThat(payload.getActivationDate(), is(dress.getActivationDate()));
        assertThat(payload.getBrand(), is(not(nullValue())));
        assertThat(payload.getImages(), hasSize(1));
        assertThat(payload.getColor(), is(dress.getColor()));
        assertThat(payload.getPrice(), is(dress.getPrice()));
        assertThat(payload.getSeason(), is(dress.getSeason()));
        assertThat(dress.getName(), is(dress.getName()));
    }

    private Dress buildDress() {
        Dress dress = new Dress();

        dress.setName("dress_name");
        dress.setPrice(new BigDecimal("900"));
        dress.setId("id");
        dress.setSeason(Season.AUTUMN);
        dress.setColor("color");
        dress.setActivationDate(new Date());
        dress.setBrand(buildBrand());
        dress.setImages(singletonList(new Image("large_url", "thumb_url")));

        return dress;
    }

    private Brand buildBrand() {
        Brand brand = new Brand();

        brand.setLogoUrl("logo_url");
        brand.setName("brand_name");

        return brand;
    }
}