package com.fashionTrade.models.dresses.payload;

import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

public class DressMessagePayloadTest {

    @Test
    public void shouldReturnTrueWhenStatusIsCreated() {
        DressMessagePayload dressMessagePayload = new DressMessagePayload(Status.CREATED, null, null, null);

        assertThat(dressMessagePayload.isCreated(), is(true));
    }

    @Test
    public void shouldReturnFalseWhenStatusIsCreated() {
        DressMessagePayload dressMessagePayload = new DressMessagePayload(Status.UPDATED, null, null, null);

        assertThat(dressMessagePayload.isCreated(), is(false));
    }

    @Test
    public void shouldHandleStatusWithNullValue() {
        DressMessagePayload dressMessagePayload = new DressMessagePayload(null, null, null, null);

        assertThat(dressMessagePayload.isCreated(), is(false));
    }
}