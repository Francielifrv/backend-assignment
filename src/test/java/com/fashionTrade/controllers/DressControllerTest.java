package com.fashionTrade.controllers;

import com.fashionTrade.models.dresses.Season;
import com.fashionTrade.models.dresses.payload.DressPayload;
import com.fashionTrade.services.DressService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import java.util.Date;

import static java.util.Collections.emptyList;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class DressControllerTest {
    private static final String DRESS_ID = "dressId";

    @Mock
    private DressService service;

    private DressController controller;

    @Before
    public void setUp() {
        initMocks(this);
        controller = new DressController(service);
    }

    @Test
    public void shouldRetrieveDressById() {
        controller.retrieveById(DRESS_ID);

        verify(service).findById(DRESS_ID);
    }

    @Test
    public void shouldReturnDressInformation() {
        DressPayload responsePayload = buildPayload();
        when(service.findById(DRESS_ID)).thenReturn(responsePayload);

        DressPayload actualPayload = controller.retrieveById(DRESS_ID);

        assertThat(actualPayload, is(responsePayload));
    }

    @Test
    public void shouldGetMostPopularDresses() {
        controller.getMostPopular();

        verify(service).getMostPopular();
    }

    private DressPayload buildPayload() {
        return new DressPayload(emptyList(), new Date(), "dressName", "color", Season.ALL, null, null, DRESS_ID);
    }
}