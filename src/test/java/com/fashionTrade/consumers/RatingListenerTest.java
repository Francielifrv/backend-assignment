package com.fashionTrade.consumers;

import com.fashionTrade.models.ratings.RatingMessagePayload;
import com.fashionTrade.models.ratings.RatingPayload;
import com.fashionTrade.services.RatingService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

public class RatingListenerTest {

    private final String DRESS_ID = "dress_id";
    private final String RATING_ID = "rating_id";
    private final int STARS = 4;
    private final Long TIMESTAMP = 1501876926000L;

    @Mock
    private RatingService ratingService;

    RatingListener ratingListener;

    @Before
    public void setUp() {
        initMocks(this);

        ratingListener = new RatingListener(ratingService);
    }

    @Test
    public void shouldUCreateNewRatingOnDatabase() {
        RatingPayload ratingPayload = new RatingPayload(RATING_ID, DRESS_ID, STARS);
        RatingMessagePayload payload = new RatingMessagePayload(ratingPayload, TIMESTAMP);

        ratingListener.consume(payload);

        verify(ratingService).insert(ratingPayload, TIMESTAMP);
    }
}