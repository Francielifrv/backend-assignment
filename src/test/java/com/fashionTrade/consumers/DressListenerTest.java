package com.fashionTrade.consumers;

import com.fashionTrade.models.dresses.Season;
import com.fashionTrade.models.dresses.payload.DressPayload;
import com.fashionTrade.models.dresses.payload.DressMessagePayload;
import com.fashionTrade.models.dresses.payload.Status;
import com.fashionTrade.services.DressService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Date;

import static java.util.Collections.emptyList;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

public class DressListenerTest {

    private static final BigDecimal PRICE = new BigDecimal("300");
    private static final DressPayload DRESS_PAYLOAD = buildDressPayload();

    private static final DressMessagePayload CREATED_PAYLOAD = buildMessagePayload(Status.CREATED);
    private static final DressMessagePayload UPDATED_PAYLOAD = buildMessagePayload(Status.UPDATED);

    @Mock
    private DressService dressService;

    private DressListener dressListener;

    @Before
    public void setUp() {
        initMocks(this);

        dressListener = new DressListener(dressService);
    }

    @Test
    public void shouldInsertDressOnDatabaseWhenMessageStatusIsCreated() {
        dressListener.consume(CREATED_PAYLOAD);

        verify(dressService).insert(CREATED_PAYLOAD.getPayload());
    }

    @Test
    public void shouldNotInsertDressOnDatabaseWhenMessageStatusIsNotCreated() {
        dressListener.consume(UPDATED_PAYLOAD);

        verify(dressService, never()).insert(any(DressPayload.class));
    }

    @Test
    public void shouldUpdateDressWhenMessageStatusIsUpdated() {
        dressListener.consume(UPDATED_PAYLOAD);

        verify(dressService).update(UPDATED_PAYLOAD.getPayload());
    }

    @Test
    public void shouldNotUpdateDressWhenMessageIsStatusIsNotUpdated() {
        dressListener.consume(CREATED_PAYLOAD);

        verify(dressService, never()).update(any(DressPayload.class));
    }

    private static DressMessagePayload buildMessagePayload(Status status) {
        return new DressMessagePayload(status, "payloadKey", DRESS_PAYLOAD, Instant.now().getEpochSecond());
    }

    private static DressPayload buildDressPayload() {
        return new DressPayload(emptyList(), new Date(), "dressName", "color", Season.ALL, PRICE, null, "9");
    }
}