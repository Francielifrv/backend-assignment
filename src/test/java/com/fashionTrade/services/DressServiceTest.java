package com.fashionTrade.services;

import com.fashionTrade.exceptions.DressNotFoundException;
import com.fashionTrade.models.dresses.Season;
import com.fashionTrade.models.dresses.document.Brand;
import com.fashionTrade.models.dresses.document.Dress;
import com.fashionTrade.models.dresses.document.Image;
import com.fashionTrade.models.dresses.payload.BrandPayload;
import com.fashionTrade.models.dresses.payload.DressPayload;
import com.fashionTrade.models.dresses.payload.ImagePayload;
import com.fashionTrade.repositories.DressRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static java.util.Collections.*;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class DressServiceTest {
    private static final String LARGE_URL = "largeUrl";
    private static final String THUMB_URL = "thumbUrl";
    private static final String DRESS_ID = "9fzZBWUBaM7qrfQDzrmU";

    private static final Date ACTIVATION_DATE = new Date();

    private static final BigDecimal PRICE = new BigDecimal("99.90");
    private static final String DRESS_NAME = "dress name";
    private static final String DRESS_COLOR = "dress color";
    private static final String BRAND_NAME = "Brand name";
    private static final String BRAND_LOGO = "www.brand-logo.com";

    @Mock
    private DressRepository repository;

    @Mock
    private RatingService ratingService;

    @Mock
    private Dress mockedDress;

    private DressPayload payload = createDressPayload();


    private DressService service;

    @Before
    public void setUp() {
        initMocks(this);

        service = new DressService(repository, ratingService);

        when(repository.findById(anyString())).thenReturn(Optional.of(mockedDress));
    }

    @Test
    public void shouldSaveDressOnDatabase() {
        service.insert(payload);

        verify(repository).save(any(Dress.class));
    }

    @Test
    public void shouldConvertPayloadToDocumentOnSave() {
        service.insert(payload);

        ArgumentCaptor<Dress> dressArgumentCaptor = ArgumentCaptor.forClass(Dress.class);

        verify(repository).save(dressArgumentCaptor.capture());

        Dress dress = dressArgumentCaptor.getValue();

        assertThat(dress.getName(), is(DRESS_NAME));
        assertThat(dress.getColor(), is(DRESS_COLOR));
        assertThat(dress.getId(), is(DRESS_ID));
        assertThat(dress.getActivationDate(), is(ACTIVATION_DATE));
        assertThat(dress.getSeason(), is(Season.AUTUMN));
        assertThat(dress.getImages(), hasSize(1));

        Image image = dress.getImages().get(0);
        assertThat(image.getLargeUrl(), is(LARGE_URL));
        assertThat(image.getThumbUrl(), is(THUMB_URL));

        Brand brand = dress.getBrand();
        assertThat(brand.getName(), is(brand.getName()));
        assertThat(brand.getLogoUrl(), is(brand.getLogoUrl()));
    }

    @Test
    public void shouldRetrieveDressForUpdate() {
        service.update(payload);

        verify(repository).findById(DRESS_ID);
    }

    @Test
    public void shouldUpdateDress() {
        service.update(payload);

        verify(mockedDress).update(payload);
        verify(repository).save(mockedDress);
    }

    @Test(expected = DressNotFoundException.class)
    public void shouldThrowDressNotFoundException() {
        when(repository.findById(anyString())).thenReturn(Optional.empty());

        service.update(payload);
    }

    @Test
    public void shouldFindDressById() {
        Dress dress = createDress();
        when(repository.findById(DRESS_ID)).thenReturn(Optional.of(dress));
        service.findById(DRESS_ID);

        verify(repository).findById(DRESS_ID);
    }

    @Test(expected = DressNotFoundException.class)
    public void shouldThrowDressNotFoundWhenRetrievingById() {
        when(repository.findById(anyString())).thenReturn(Optional.empty());

        service.findById("invalidDressId");
    }

    @Test
    public void shouldReturnDressPayload() {
        Dress dress = createDress();
        when(repository.findById(DRESS_ID)).thenReturn(Optional.of(dress));

        DressPayload retrievedDress = service.findById(DRESS_ID);

        assertThat(retrievedDress.getId(), is(DRESS_ID));
        assertThat(retrievedDress.getName(), is(DRESS_NAME));
        assertThat(retrievedDress.getSeason(), is(Season.SPRING));
        assertThat(retrievedDress.getPrice(), is(PRICE));
    }

    @Test
    public void shouldGetMostRatedDresses() {
        service.getMostPopular();

        verify(ratingService).getMostPopularDressIds();
    }

    @Test
    public void shouldRetrieveMostRatedDressesById() {
        Set<String> dressIds = singleton(DRESS_ID);
        when(ratingService.getMostPopularDressIds()).thenReturn(dressIds);

        service.getMostPopular();

        ArgumentCaptor<Set> idsArgumentCaptor = ArgumentCaptor.forClass(Set.class);
        verify(repository).findAllById(idsArgumentCaptor.capture());

        assertThat(idsArgumentCaptor.getValue(), is(dressIds));
    }

    @Test
    public void shouldRetrieveListOfMostPopularDresses() {
        Dress dress = createDress();
        when(repository.findAllById(any())).thenReturn(singletonList(dress));

        List<DressPayload> popularDresses = service.getMostPopular();

        assertThat(popularDresses, hasSize(1));
    }

    private DressPayload createDressPayload() {
        List<ImagePayload> images = singletonList(new ImagePayload(LARGE_URL, THUMB_URL));
        BrandPayload brand = new BrandPayload(BRAND_NAME, BRAND_LOGO);

        return new DressPayload(images, ACTIVATION_DATE, DRESS_NAME, DRESS_COLOR, Season.AUTUMN, PRICE, brand, DRESS_ID);
    }

    private Dress createDress() {
        Dress dress = new Dress();

        dress.setImages(emptyList());
        dress.setColor(DRESS_COLOR);
        dress.setPrice(PRICE);
        dress.setSeason(Season.SPRING);
        dress.setId(DRESS_ID);
        dress.setName(DRESS_NAME);
        dress.setBrand(new Brand());

        return dress;
    }
}