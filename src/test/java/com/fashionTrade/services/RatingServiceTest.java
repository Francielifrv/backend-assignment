package com.fashionTrade.services;

import com.fashionTrade.models.ratings.Rating;
import com.fashionTrade.models.ratings.RatingPayload;
import com.fashionTrade.repositories.RatingRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;

import java.util.*;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class RatingServiceTest {
    private static final String RATING_ID = "ratingId";
    private static final String DRESS_ID = "dressId";
    private static final int STARS = 4;
    private final long TIMESTAMP = 1487596717302L;

    @Mock
    private RatingRepository repository;

    private RatingService service;

    @Before
    public void setUp() {
        initMocks(this);

        service = new RatingService(repository);
    }

    @Test
    public void shouldSaveRateOnDatabase() {
        RatingPayload payload = new RatingPayload(RATING_ID, DRESS_ID, STARS);

        service.insert(payload, TIMESTAMP);

        ArgumentCaptor<Rating> ratingArgumentCaptor = ArgumentCaptor.forClass(Rating.class);
        verify(repository).save(ratingArgumentCaptor.capture());

        Rating rating = ratingArgumentCaptor.getValue();

        assertThat(rating.getDressId(), is(DRESS_ID));
        assertThat(rating.getRatingId(), is(RATING_ID));
        assertThat(rating.getStars(), is(STARS));
        assertThat(rating.getRatedAt(), is(new Date(TIMESTAMP)));
    }

    @Test
    public void shouldGetMostRatedDresses() {
        service.getMostPopularDressIds();

        verify(repository).findMostPopularDressIds();
    }

    @Test
    public void shouldReturnListOfMostPopularDresses() {
        Set<String> mostPopularDressIds = new HashSet<>(Arrays.asList(DRESS_ID, "popularDressId", "anotherPopularDressId"));
        when(repository.findMostPopularDressIds()).thenReturn(mostPopularDressIds);

        Set<String> dressIds = service.getMostPopularDressIds();

        assertThat(dressIds, is(mostPopularDressIds));
    }
}