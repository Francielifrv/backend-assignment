package com.fashionTrade.repositories;

import com.fashionTrade.models.dresses.document.Dress;

import java.util.List;

public interface DressRepositoryCustom {
    List<Dress> searchDresses(String token);
}
