package com.fashionTrade.repositories;

import com.fashionTrade.models.dresses.document.Dress;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.common.unit.Fuzziness;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class DressRepositoryImpl implements DressRepositoryCustom {

    private final Client client;

    @Autowired
    public DressRepositoryImpl(Client client) {
        this.client = client;
    }

    @Override
    public List<Dress> searchDresses(String token) {
        QueryBuilder query = QueryBuilders.multiMatchQuery(token, "name", "color", "brand.name")
                .fuzziness(Fuzziness.ONE);

        SearchResponse response = client.prepareSearch("dress").setQuery(query).execute().actionGet();

        return fromSearchResponse(response);
    }

    private List<Dress> fromSearchResponse(SearchResponse searchResponse) {
       return Arrays.stream(searchResponse.getHits().getHits()).map(hit -> {
            Dress dress = new Dress();
            try {
                dress = fromJson(hit.getSourceAsString());
            } catch (IOException e) {
                e.printStackTrace();
            }

            return dress;
        }).collect(Collectors.toList());
    }

    private Dress fromJson(String jsonPayload) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();

        return objectMapper.readValue(jsonPayload, Dress.class);
    }

}
