package com.fashionTrade.repositories;

import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.MultiBucketsAggregation;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.bucket.terms.TermsAggregationBuilder;
import org.elasticsearch.search.sort.FieldSortBuilder;
import org.elasticsearch.search.sort.SortBuilders;
import org.elasticsearch.search.sort.SortMode;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Set;
import java.util.stream.Collectors;

import static org.elasticsearch.index.query.QueryBuilders.matchAllQuery;

public class RatingRepositoryImpl implements RatingRepositoryCustom {

    private final Client client;

    @Autowired
    public RatingRepositoryImpl(Client client) {
        this.client = client;
    }

    @Override
    public Set<String> findMostPopularDressIds() {
        TermsAggregationBuilder aggregations = buildRequestAggregations();

        FieldSortBuilder sort = getSort();

        SearchResponse response = client.prepareSearch("rating")
                .setQuery(matchAllQuery())
                .addAggregation(aggregations)
                .addSort(sort).execute().actionGet();

        return extractDressIds(response);
    }

    private FieldSortBuilder getSort() {
        return SortBuilders.fieldSort("stars")
                    .order(SortOrder.ASC)
                    .sortMode(SortMode.SUM);
    }

    private TermsAggregationBuilder buildRequestAggregations() {
        return AggregationBuilders.terms("by_dressId")
                .field("dressId.keyword")
                .subAggregation(AggregationBuilders
                        .sum("sum_stars")
                        .field("stars"));
    }

    private Set<String> extractDressIds(SearchResponse response) {
        Terms terms = response.getAggregations().get("by_dressId");
        return terms.getBuckets().stream()
                .map(MultiBucketsAggregation.Bucket::getKeyAsString)
                .collect(Collectors.toSet());
    }
}