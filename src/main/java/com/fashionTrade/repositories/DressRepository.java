package com.fashionTrade.repositories;

import com.fashionTrade.models.dresses.document.Dress;
import org.springframework.data.elasticsearch.repository.ElasticsearchCrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DressRepository extends DressRepositoryCustom, ElasticsearchCrudRepository<Dress, String> {
}
