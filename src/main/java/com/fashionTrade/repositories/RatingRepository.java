package com.fashionTrade.repositories;

import com.fashionTrade.models.ratings.Rating;
import org.springframework.data.elasticsearch.repository.ElasticsearchCrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RatingRepository extends RatingRepositoryCustom, ElasticsearchCrudRepository<Rating, String> {
}
