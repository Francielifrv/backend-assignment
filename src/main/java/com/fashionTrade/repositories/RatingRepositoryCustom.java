package com.fashionTrade.repositories;

import java.util.Set;

public interface RatingRepositoryCustom {
    Set<String> findMostPopularDressIds();
}