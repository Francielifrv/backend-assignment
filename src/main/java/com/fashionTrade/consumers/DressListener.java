package com.fashionTrade.consumers;

import com.fashionTrade.models.dresses.payload.DressMessagePayload;
import com.fashionTrade.services.DressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class DressListener {

    private final DressService service;

    @Autowired
    public DressListener(DressService service) {
        this.service = service;
    }

    @KafkaListener(topics = "${kafka.topic.dresses.name}", groupId = "${kafka.topic.dresses.group-id}",
            containerFactory = "kafkaListenerContainerFactory")
    public void consume(DressMessagePayload dressMessagePayload) {

        if (dressMessagePayload.isCreated()) {
            service.insert(dressMessagePayload.getPayload());
        } else {
            service.update(dressMessagePayload.getPayload());
        }
    }
}
