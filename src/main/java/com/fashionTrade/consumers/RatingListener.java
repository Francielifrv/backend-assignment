package com.fashionTrade.consumers;

import com.fashionTrade.models.ratings.RatingMessagePayload;
import com.fashionTrade.services.RatingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class RatingListener {
    private final RatingService service;

    @Autowired
    public RatingListener(RatingService service) {
        this.service = service;
    }

    @KafkaListener(topics = "${kafka.topic.ratings.name}", groupId = "${kafka.topic.ratings.group-id}",
            containerFactory = "kafkaListenerContainerFactoryRating")
    public void consume(RatingMessagePayload messagePayload) {
        service.insert(messagePayload.getPayload(), messagePayload.getTimestamp());
    }
}
