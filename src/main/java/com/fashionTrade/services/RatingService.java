package com.fashionTrade.services;

import com.fashionTrade.models.ratings.Rating;
import com.fashionTrade.models.ratings.RatingPayload;
import com.fashionTrade.repositories.RatingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class RatingService {

    private final RatingRepository repository;

    @Autowired
    public RatingService(RatingRepository repository) {
        this.repository = repository;
    }

    public void insert(RatingPayload payload, Long timestamp) {
        Rating rating = new Rating(payload, timestamp);

        repository.save(rating);
    }

    public Set<String> getMostPopularDressIds() {
        return repository.findMostPopularDressIds();
    }
}