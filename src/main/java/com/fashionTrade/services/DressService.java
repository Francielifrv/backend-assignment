package com.fashionTrade.services;

import com.fashionTrade.exceptions.DressNotFoundException;
import com.fashionTrade.models.dresses.document.Dress;
import com.fashionTrade.models.dresses.payload.DressPayload;
import com.fashionTrade.repositories.DressRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
@Transactional
public class DressService {

    private final DressRepository dressRepository;
    private final RatingService ratingService;

    private static final Logger LOGGER = LoggerFactory.getLogger(DressService.class);

    @Autowired
    public DressService(DressRepository dressRepository, RatingService ratingService) {
        this.dressRepository = dressRepository;
        this.ratingService = ratingService;
    }

    public void insert(DressPayload payload) {
        Dress dress = new Dress(payload);

        dressRepository.save(dress);
    }

    public void update(DressPayload payload) {
        Dress dress = findDressById(payload.getId());

        dress.update(payload);

        dressRepository.save(dress);

        LOGGER.info("Updated dress with id " + dress.getId());
    }

    public DressPayload findById(String id) {
        Dress dress = findDressById(id);

        return new DressPayload(dress);
    }

    private Dress findDressById(String dressId) {
        return dressRepository.findById(dressId)
                .orElseThrow(() -> new DressNotFoundException("Could not find dress with id - " + dressId));
    }

    public List<DressPayload> getMostPopular() {
        Set<String> dressIds = ratingService.getMostPopularDressIds();

        Iterable<Dress> popularDresses = dressRepository.findAllById(dressIds);

        return convertToDressPayloadList(popularDresses);
    }

    public List<DressPayload> retrieveAll() {
        Iterable<Dress> dresses = dressRepository.findAll();

        return convertToDressPayloadList(dresses);
    }

    public List<DressPayload> searchBy(String token) {
        List<Dress> dresses = dressRepository.searchDresses(token);

        return convertToDressPayloadList(dresses);
    }

    private List<DressPayload> convertToDressPayloadList(Iterable<Dress> popularDresses) {
        return StreamSupport.stream(popularDresses.spliterator(), false)
                .map(DressPayload::new)
                .collect(Collectors.toList());
    }
}