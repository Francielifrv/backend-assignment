package com.fashionTrade.models.dresses.payload;

public enum Status {
    CREATED, UPDATED
}
