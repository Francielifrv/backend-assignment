package com.fashionTrade.models.dresses.payload;

import com.fashionTrade.models.dresses.document.Image;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ImagePayload {
    private final String largeUrl;
    private final String thumbUrl;

    @JsonCreator
    public ImagePayload(@JsonProperty("large_url") String largeUrl, @JsonProperty("thumb_url") String thumbUrl) {
        this.largeUrl = largeUrl;
        this.thumbUrl = thumbUrl;
    }

    public ImagePayload(Image image) {
        this(image.getLargeUrl(), image.getThumbUrl());
    }

    public String getLargeUrl() {
        return largeUrl;
    }

    public String getThumbUrl() {
        return thumbUrl;
    }
}
