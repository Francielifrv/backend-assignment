package com.fashionTrade.models.dresses.payload;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class DressMessagePayload {
    private final Status status;
    private final String payloadKey;
    private final DressPayload payload;
    private final Long timestamp;

    @JsonCreator
    public DressMessagePayload(@JsonProperty("status") Status status, @JsonProperty("payload_key") String payloadKey,
                               @JsonProperty("payload") DressPayload payload, @JsonProperty("timestamp") Long timestamp) {
        this.status = status;
        this.payloadKey = payloadKey;
        this.payload = payload;
        this.timestamp = timestamp;
    }

    public DressPayload getPayload() {
        return payload;
    }

    public boolean isCreated() {
        return Status.CREATED.equals(this.status);
    }
}