package com.fashionTrade.models.dresses.payload;

import com.fashionTrade.models.dresses.document.Brand;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class BrandPayload {
    private final String name;
    private final String logoUrl;

    @JsonCreator
    public BrandPayload(@JsonProperty("name") String name, @JsonProperty("logo_url") String logoUrl) {
        this.name = name;
        this.logoUrl = logoUrl;
    }

    public BrandPayload(Brand brand) {
        this(brand.getName(), brand.getLogoUrl());
    }

    public String getName() {
        return name;
    }

    public String getLogoUrl() {
        return logoUrl;
    }
}