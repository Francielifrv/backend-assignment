package com.fashionTrade.models.dresses.payload;

import com.fashionTrade.models.dresses.Season;
import com.fashionTrade.models.dresses.document.Dress;
import com.fashionTrade.models.dresses.document.Image;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class DressPayload {
    private final List<ImagePayload> images;
    private final Date activationDate;
    private final String name;
    private final String color;
    private final Season season;
    private final BigDecimal price;
    private final BrandPayload brand;
    private final String id;

    @JsonCreator
    public DressPayload(@JsonProperty("images") List<ImagePayload> images, @JsonProperty("activation_date") Date activationDate,
                        @JsonProperty("name") String name, @JsonProperty("color") String color,
                        @JsonProperty("season") Season season, @JsonProperty("price") BigDecimal price,
                        @JsonProperty("brand") BrandPayload brand, @JsonProperty("id") String id) {
        this.images = images;
        this.activationDate = activationDate;
        this.name = name;
        this.color = color;
        this.season = season;
        this.price = price;
        this.brand = brand;
        this.id = id;
    }

    public DressPayload(Dress dress) {
        this.images = buildImageList(dress.getImages());
        this.activationDate = dress.getActivationDate();
        this.name = dress.getName();
        this.color = dress.getColor();
        this.season = dress.getSeason();
        this.price = dress.getPrice();
        this.brand = new BrandPayload(dress.getBrand());
        this.id = dress.getId();
    }

    private List<ImagePayload> buildImageList(List<Image> images) {
        return images.stream().map(ImagePayload::new).collect(Collectors.toList());
    }

    public List<ImagePayload> getImages() {
        return images;
    }

    public Date getActivationDate() {
        return activationDate;
    }

    public String getName() {
        return name;
    }

    public String getColor() {
        return color;
    }

    public Season getSeason() {
        return season;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public BrandPayload getBrand() {
        return brand;
    }

    public String getId() {
        return id;
    }
}
