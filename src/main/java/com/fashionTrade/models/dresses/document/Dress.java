package com.fashionTrade.models.dresses.document;

import com.fashionTrade.models.dresses.Season;
import com.fashionTrade.models.dresses.payload.DressPayload;
import com.fashionTrade.models.dresses.payload.ImagePayload;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Document(indexName = "dress")
public class Dress {

    @Id
    private String id;

    @Field(type = FieldType.Nested)
    private List<Image> images;
    private Date activationDate;
    private String name;
    private String color;
    private Season season;
    private BigDecimal price;

    @Field(type = FieldType.Nested)
    private Brand brand;

    public Dress(DressPayload dressPayload) {
        fromPayload(dressPayload);
    }

    public Dress() {

    }

    public String getId() {
        return id;
    }

    public List<Image> getImages() {
        return images;
    }

    public Date getActivationDate() {
        return activationDate;
    }

    public String getName() {
        return name;
    }

    public String getColor() {
        return color;
    }

    public Season getSeason() {
        return season;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public Brand getBrand() {
        return brand;
    }

    public void update(DressPayload dressPayload) {
        fromPayload(dressPayload);
    }

    private void fromPayload(DressPayload dressPayload) {
        this.id = dressPayload.getId();
        this.name = dressPayload.getName();
        this.color = dressPayload.getColor();
        this.season = dressPayload.getSeason();
        this.price = dressPayload.getPrice();
        this.activationDate = dressPayload.getActivationDate();
        this.brand = new Brand(dressPayload.getBrand());
        this.images = convertImageFromPayload(dressPayload.getImages());
    }

    private static List<Image> convertImageFromPayload(List<ImagePayload> images) {
        return images.stream().map(Image::new).collect(Collectors.toList());
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setImages(List<Image> images) {
        this.images = images;
    }

    public void setActivationDate(Date activationDate) {
        this.activationDate = activationDate;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setSeason(Season season) {
        this.season = season;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public void setBrand(Brand brand) {
        this.brand = brand;
    }
}