package com.fashionTrade.models.dresses.document;

import com.fashionTrade.models.dresses.payload.BrandPayload;

public class Brand {
    private String name;
    private String logoUrl;

    public Brand(BrandPayload brandPayload) {
        this.name = brandPayload.getName();
        this.logoUrl = brandPayload.getLogoUrl();
    }

    public Brand() {
    }

    public String getName() {
        return name;
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }
}