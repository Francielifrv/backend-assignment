package com.fashionTrade.models.dresses.document;

import com.fashionTrade.models.dresses.payload.ImagePayload;

public class Image {
    private String largeUrl;
    private String thumbUrl;

    public Image(ImagePayload imagePayload) {
        this.largeUrl = imagePayload.getLargeUrl();
        this.thumbUrl = imagePayload.getThumbUrl();
    }

    public Image() {
    }

    public String getLargeUrl() {
        return largeUrl;
    }

    public String getThumbUrl() {
        return thumbUrl;
    }

    public Image(String largeUrl, String thumbUrl) {
        this.largeUrl = largeUrl;
        this.thumbUrl = thumbUrl;
    }
}
