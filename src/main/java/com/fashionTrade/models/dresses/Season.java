package com.fashionTrade.models.dresses;

public enum  Season {
    SUMMER, WINTER, SPRING, AUTUMN, ALL
}
