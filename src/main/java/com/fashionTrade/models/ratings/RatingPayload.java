package com.fashionTrade.models.ratings;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class RatingPayload {
    private final String ratingId;
    private final String dressId;
    private final int stars;

    @JsonCreator
    public RatingPayload(@JsonProperty("rating_id") String ratingId, @JsonProperty("dress_id") String dressId,
                         @JsonProperty("stars") int stars) {
        this.ratingId = ratingId;
        this.dressId = dressId;
        this.stars = stars;
    }

    public String getRatingId() {
        return ratingId;
    }

    public String getDressId() {
        return dressId;
    }

    public int getStars() {
        return stars;
    }
}
