package com.fashionTrade.models.ratings;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class RatingMessagePayload {
    private final RatingPayload payload;
    private final Long timestamp;

    @JsonCreator
    public RatingMessagePayload(@JsonProperty("payload") RatingPayload payload, @JsonProperty("timestamp") Long timestamp) {
        this.payload = payload;
        this.timestamp = timestamp;
    }

    public RatingPayload getPayload() {
        return payload;
    }

    public Long getTimestamp() {
        return timestamp;
    }
}