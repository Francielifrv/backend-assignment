package com.fashionTrade.models.ratings;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

import java.util.Date;

@Document(indexName = "rating")
public class Rating {

    @Id
    private String id;
    private int stars;
    private Date ratedAt;
    private String dressId;

    public Rating(RatingPayload payload, Long timestamp) {
        this.id = payload.getRatingId();
        this.stars = payload.getStars();
        this.ratedAt = new Date(timestamp);
        this.dressId = payload.getDressId();
    }

    public Rating() {
    }

    public String getRatingId() {
        return id;
    }

    public int getStars() {
        return stars;
    }

    public Date getRatedAt() {
        return ratedAt;
    }

    public String getDressId() {
        return dressId;
    }
}
