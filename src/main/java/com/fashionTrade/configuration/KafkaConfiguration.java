package com.fashionTrade.configuration;

import com.fashionTrade.models.dresses.payload.DressMessagePayload;
import com.fashionTrade.models.ratings.RatingMessagePayload;
import com.google.common.collect.ImmutableMap;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.support.serializer.JsonDeserializer;

import java.util.Map;

@Configuration
@EnableKafka
public class KafkaConfiguration {

    @Value("${kafka.topic.dresses.group-id}")
    private String dressTopicGroupId;

    @Value("${kafka.topic.ratings.group-id}")
    private String ratingGroupId;

    @Value("${kafka.bootstrap-servers}")
    private String bootstrapServer;

    @Bean
    public ConsumerFactory<String, DressMessagePayload> consumerFactory() {
        return new DefaultKafkaConsumerFactory(consumerConfiguration(dressTopicGroupId), new StringDeserializer(),
                new JsonDeserializer<>(DressMessagePayload.class));
    }

    private Map<String, Object> consumerConfiguration(String groupId) {
        return ImmutableMap.of(
                ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServer,
                ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class,
                ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, JsonDeserializer.class
        );
    }

    @Bean
    public ConcurrentKafkaListenerContainerFactory<String, DressMessagePayload> kafkaListenerContainerFactory() {
        ConcurrentKafkaListenerContainerFactory<String, DressMessagePayload> factory = new ConcurrentKafkaListenerContainerFactory();
        factory.setConsumerFactory(consumerFactory());

        return factory;
    }

    @Bean
    public ConsumerFactory<String, RatingMessagePayload> consumerFactoryRating() {
        return new DefaultKafkaConsumerFactory(consumerConfiguration(ratingGroupId), new StringDeserializer(),
                new JsonDeserializer<>(RatingMessagePayload.class));
    }

    @Bean
    public ConcurrentKafkaListenerContainerFactory<String, RatingMessagePayload> kafkaListenerContainerFactoryRating() {
        ConcurrentKafkaListenerContainerFactory<String, RatingMessagePayload> factory = new ConcurrentKafkaListenerContainerFactory();
        factory.setConsumerFactory(consumerFactoryRating());

        return factory;
    }
}
