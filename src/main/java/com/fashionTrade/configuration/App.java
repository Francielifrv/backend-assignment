package com.fashionTrade.configuration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.fashionTrade")
public class App {
    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }
}
