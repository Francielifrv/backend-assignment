package com.fashionTrade.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class DressNotFoundException extends RuntimeException {

    public DressNotFoundException(String message) {
        super(message);
    }
}