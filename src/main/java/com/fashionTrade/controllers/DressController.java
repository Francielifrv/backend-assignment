package com.fashionTrade.controllers;

import com.fashionTrade.models.dresses.payload.DressPayload;
import com.fashionTrade.services.DressService;
import org.apache.kafka.common.protocol.types.Field;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.awt.print.Pageable;
import java.util.List;

@RestController
public class DressController {

    private final DressService service;

    @Autowired
    public DressController(DressService service) {
        this.service = service;
    }

    @GetMapping("/dresses/{id}")
    @ResponseStatus(HttpStatus.OK)
    public DressPayload retrieveById(@PathVariable("id") String id) {
        return service.findById(id);
    }

    @GetMapping("/dresses/most-popular")
    @ResponseStatus(HttpStatus.OK)
    public List<DressPayload> getMostPopular() {
        return service.getMostPopular();
    }

    @GetMapping("dresses/search")
    @ResponseStatus(HttpStatus.OK)
    public List<DressPayload> searchBy(@RequestParam("token") String token){
        return service.searchBy(token);
    }

    @GetMapping("dresses")
    @ResponseStatus(HttpStatus.OK)
    public List<DressPayload> retrieveAll(){
        return service.retrieveAll();
    }
}